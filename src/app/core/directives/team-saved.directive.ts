import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { StateService } from '../services/state-service.service';
import { Subject, distinctUntilChanged, map, takeUntil, tap } from 'rxjs';

@Directive({
  selector: '[isTeamSaved]',
  standalone: true
})
export class TeamSavedDirective implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  constructor(private stateService: StateService,
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>) {

  }
  ngOnInit(): void {
    this.stateService.myTeam$.pipe(takeUntil(this.destroy$), map((data) => {
      return Boolean(data && data.saved)}), distinctUntilChanged(), tap((isSaved) => {
      !isSaved ? this.viewContainerRef.createEmbeddedView(this.templateRef) : this.viewContainerRef.clear()
    })).subscribe()
    // throw new Error('Method not implemented.');
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();

    // throw new Error('Method not implemented.');
  }
}
