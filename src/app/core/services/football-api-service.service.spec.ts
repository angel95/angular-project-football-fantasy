import { TestBed } from '@angular/core/testing';

import { FootballApiService } from './football-api-service.service';
import { HttpClientModule } from '@angular/common/http';

describe('FootballApiService', () => {
  let service: FootballApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(FootballApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
