import { Injectable, inject } from '@angular/core';
import { Observable, lastValueFrom, map, take } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { ApiResponse, ICoach, IPlayersResponse, ITeam, ITeamsResponse } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class FootballApiService {
  private http = inject(HttpClient);

  constructor() { }

  getAllTeams(): Observable<ApiResponse<ITeamsResponse[]>> {
    return this.http.get<ApiResponse<ITeamsResponse[]>>(`${environment.apiUrl}/teams?league=10&season=2022`).pipe(map((response) => {
      response.response = response.response.filter((team) => team.team.national == true)
      return response;
    }));
  }

  getPlayersByTeam(team: ITeam, page: number, search: ''): Observable<ApiResponse<IPlayersResponse[]>> {
    return this.http.get<ApiResponse<IPlayersResponse[]>>
      (`${environment.apiUrl}/players?league=10&season=2022&team=${team.id}&page${page}&search${search}`)
        .pipe(map((response) => {
          return response;
    }));
  }

  getCoachByName(name: string): Observable<ApiResponse<ICoach[]>> {
    return this.http.get<ApiResponse<ICoach[]>>(`${environment.apiUrl}/coachs?search=${name}`).pipe(map((response) => {
      return response;
    }));
  }

  async getPlayersByTeamPromise(team: ITeam, page: number, search = ''): Promise<ApiResponse<IPlayersResponse[]>> {
    const searchQuery = search != '' ? `&search=${search}` : '';

    const request$: Observable<ApiResponse<IPlayersResponse[]>> =
      this.http.get<ApiResponse<IPlayersResponse[]>>(`${environment.apiUrl}/players?league=10&season=2022&team=${team.id}&page=${page}${searchQuery}`).pipe(take(1));

    return await lastValueFrom<ApiResponse<IPlayersResponse[]>>(request$);
  }

}
