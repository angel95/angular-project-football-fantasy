import { Injectable, inject } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { ICoach, ITeam } from '../interfaces';
import { TeamApp } from '../models/team.model';
import { PlayerApp } from '../models/player/player.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LocalStorageService } from './local-storage.service';
import { IPlayerApp } from '../interfaces/player-app.interface';
import { PlayerMapper } from '../models/player/player.mapper';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  private localStorageService = inject(LocalStorageService);
  private isTeamSelected = new BehaviorSubject<boolean>(false);
  isTeamSelected$ = this.isTeamSelected.asObservable();

  private teamSelected = new BehaviorSubject<ITeam | null>(null);
  teamSelected$ = this.teamSelected.asObservable();

  private myTeam = new BehaviorSubject<TeamApp>(new TeamApp('', null, []));
  myTeam$ = this.myTeam.asObservable();

  playerMapper = new PlayerMapper();

  constructor(private _snackBar: MatSnackBar) {
  }

  public switchStateTeamSelection(): void {
    this.isTeamSelected.next(!this.isTeamSelected.value)
  }

  public setTeam(team: ITeam): void {
    this.teamSelected.next(team);
  }

  public addPlayerToTeam(player: PlayerApp): void {
    let actualTeam = this.myTeam.value;

    if (actualTeam.contraints.maxSize <= actualTeam.players.length) {
      this.openSnackBar(`Max ${actualTeam.contraints.maxSize} players on the team`, true);
      return;
    }
    if (actualTeam.isAlreadyOnTheTeam(player.id)) {
      this.openSnackBar(`This player is on the team!`, true);
      return;
    }
    if (actualTeam.amountPlayersPerTeam(player.team) >= 4) {
      this.openSnackBar(`You can't add more players from this selection!`, true);
      return;
    }

    actualTeam.addPlayer(player);
    this.openSnackBar(`Player ${player.name} added to the Team!`);
    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
    return;

  }

  public addCoach(coach: ICoach): void {
    let actualTeam = this.myTeam.value;
    actualTeam.setCoach(coach);
    this.openSnackBar(`Coach ${coach.name} selected!`);
    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
    return;
  }

  removePlayerFromTeam(player: PlayerApp): void {
    let actualTeam = this.myTeam.value;
    actualTeam.removePlayerFromTeam(player);

    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
  }

  removeCoachFromTeam(): void {
    let actualTeam = this.myTeam.value;
    actualTeam.setCoach(null);

    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
  }

  getTeamFromStorage(data: string): void {
    const datajson = JSON.parse(data);
    const players: PlayerApp[] = datajson['players'].map((player: IPlayerApp) => {
      return this.playerMapper.mapFromInterface(player)
    })
    this.myTeam.next(new TeamApp(datajson['name'], datajson['coach'], players, datajson['saved']));
    return;
  }

  openSnackBar(message: string, isError: boolean = false): void {
    this._snackBar.open(message, undefined, {
      duration: 2000,
      panelClass: isError ? 'error-snackbar' : ''
    });
  }

  setTeamName(teamName: string): void {
    let actualTeam = this.myTeam.value;
    actualTeam.setName(teamName);

    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
  }

  saveTeam(): void {
    let actualTeam = this.myTeam.value;
    // actualTeam.changeStatus();

    if (actualTeam.amountPlayersInPosition('Attacker') < actualTeam.contraints.minPerPosition['Attacker']) {
      this.openSnackBar(`Min ${actualTeam.contraints.minPerPosition['Attacker']} Attackers`, true);
      return;
    } else if (actualTeam.amountPlayersInPosition('Midfielder') < actualTeam.contraints.minPerPosition['Midfielder']) {
      this.openSnackBar(`Min ${actualTeam.contraints.minPerPosition['Midfielder']} Midfielders`, true);
      return;
    } else if (actualTeam.amountPlayersInPosition('Defender') < actualTeam.contraints.minPerPosition['Defender']) {
      this.openSnackBar(`Min ${actualTeam.contraints.minPerPosition['Defender']} Defenders`, true);
      return;
    } else if (actualTeam.amountPlayersInPosition('Goalkeeper') < actualTeam.contraints.minPerPosition['Goalkeeper']) {
      this.openSnackBar(`Min ${actualTeam.contraints.minPerPosition['Goalkeeper']} Goalkeepers`, true);
      return;
    } else if (!actualTeam.coach) {
      this.openSnackBar(`Please select a coach for the Team`, true);
      return;
    } else if (actualTeam.name === '') {
      this.openSnackBar(`Please type a name for the Team`, true);
      return;
    }

    actualTeam.changeStatus();
    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);

  }

  editTeam(): void {
    let actualTeam = this.myTeam.value;
    actualTeam.changeStatus();
    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
  }

  deleteTeam(): void {
    let actualTeam = new TeamApp('', null, []);
    this.localStorageService.setItem('myTeam', actualTeam.toString());
    this.myTeam.next(actualTeam);
  }

}
