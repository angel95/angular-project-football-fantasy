import { TestBed } from '@angular/core/testing';

import { StateService } from './state-service.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('StateServiceService', () => {
  let service: StateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule]
    });
    service = TestBed.inject(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
