import { ICoach, ITeamConstraints, PlayerPosition } from '../interfaces';
import { PlayerApp } from './player/player.model';

export class TeamApp {

  private _name : string;

  private _coach : ICoach | null;

  private _players : PlayerApp[] = [];

  private _saved : boolean;

  public contraints: ITeamConstraints = {
    maxSize: 16,
    minPerPosition: {
      'Attacker': 4,
      'Midfielder': 4,
      'Defender': 4,
      'Goalkeeper': 2,
    }
  };

  constructor(
    name: string,
    coach: ICoach | null,
    players: PlayerApp[],
    saved = false
  ){
    this._name = name;
    this._coach = coach;
    this._players = players;
    this._saved = saved
  }

  public get players() : PlayerApp[] {
    return this._players;
  }
  public set players(v : PlayerApp[]) {
    this._players = v;
  }

  public get name() : string {
    return this._name;
  }
  public setName(v : string) {
    this._name = v;
  }

  public get coach() : ICoach | null {
    return this._coach;
  }
  public setCoach(coach : ICoach| null) {
    this._coach = coach;
  }

  public addPlayer(player: PlayerApp): void {
    this._players.push(player);
  }

  public get saved() : boolean {
    return this._saved;
  }

  public amountPlayersInPosition(position: PlayerPosition): number {
    return this.players.filter(player => player.position == position).length
  }

  public playersByPosition(position: PlayerPosition): PlayerApp[] {
    return this.players.filter(player => player.position == position);
  }
  public amountPlayersPerTeam(teamName: string): number {
    return this.players.filter(player => player.team == teamName).length
  }

  public isAlreadyOnTheTeam(playerID: number): boolean{
    return this.players.some(player => player.id == playerID);
  }

  public removePlayerFromTeam(player: PlayerApp): void{
    this._players = this._players.filter((playerArray) => player.id != playerArray.id);
  }

  public toString(): string {
    return `{"name":"${this._name}", "coach":${JSON.stringify(this._coach)}, "saved": ${this._saved}, "players":[${this._players.toString()}]}`
  }

  public contraintsString(): string {
    return `Max ${this.contraints.maxSize} players (${this._players.length}/${this.contraints.maxSize})`
  }

  public changeStatus(): void {
    this._saved = !this.saved;
  }

}
