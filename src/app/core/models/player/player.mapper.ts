import { Mapper } from "../../base/mapper";
import { IPlayersResponse, PlayerPosition } from "../../interfaces";
import { IPlayerApp } from "../../interfaces/player-app.interface";
import { PlayerApp } from './player.model';

export class PlayerMapper extends Mapper<IPlayersResponse, PlayerApp, IPlayerApp>{
  mapFrom(param: IPlayersResponse): PlayerApp {

    const position: PlayerPosition = param.statistics[0].games.position;

    const teamName: string = param.statistics[0].team.name;
    const teamLogo: string = param.statistics[0].team.logo;

    return new PlayerApp(param.player.age, param.player.id, param.player.name, param.player.photo, position, teamName, teamLogo);
  }

  // mapFromInterface(data: )


  mapFromInterface(player: IPlayerApp): PlayerApp {
    return new PlayerApp(
      player.age, player.id, player.name, player.photo, player.position, player.team, player.teamLogo
    );
  }
}
