import { PlayerPosition } from "../../interfaces";
import { IPlayerApp } from "../../interfaces/player-app.interface";

export class PlayerApp {
  private _age: number;
  private _id: number;
  private _name: string;
  private _photo: string;
  private _position: PlayerPosition;
  private _team: string;
  private _teamLogo: string;

  constructor(
    age: number,
    id: number,
    name: string,
    photo: string,
    position: PlayerPosition,
    team: string,
    teamLogo: string,
  ) {
    this._age = age;
    this._id = id;
    this._name = name;
    this._photo = photo;
    this._position = position;
    this._team = team;
    this._teamLogo = teamLogo
  }

  public get age(): number {
    return this._age;
  }


  public get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get photo(): string {
    return this._photo;
  }

  public get position(): PlayerPosition {
    return this._position;
  }

  public get team(): string {
    return this._team;
  }

  public get teamLogo(): string {
    return this._teamLogo;
  }

  public toString(): string {
    return `{"age": "${this._age}","id": "${this._id}","name": "${this._name}","photo": "${this._photo}","position": "${this._position}","team": "${this._team}","teamLogo": "${this._teamLogo}"}`
  }

}

