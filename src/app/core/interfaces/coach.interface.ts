export interface ICoach {
  id: number;
  name: string;
  firstname: string,
  lastname: string,
  age: number,
  nationality: string,
  height: string,
  weight: string,
  photo: string,
  position: null;
}
