import { PlayerPosition } from "./player.interface";

export interface IPlayerApp {
  age: number;
  id: number;
  name: string;
  photo: string;
  position: PlayerPosition;
  team: string;
  teamLogo : string;
}
