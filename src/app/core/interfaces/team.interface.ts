import { PlayerPosition } from "./player.interface"

export interface ITeam{
  id: number
  name: String
  code: String
  country: String
  founded: number
  national: boolean
  logo: string
}

export interface ITeamConstraints{
  maxSize: number;
  minPerPosition: ConstraintMinPerPosition;
}

export interface ConstraintMinPerPosition {
  [key: string | PlayerPosition]: number;
}
