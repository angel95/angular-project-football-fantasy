import { IPlayer, IStatistics } from './player.interface';
import { ITeam } from './team.interface';
import { IVenue } from './venue.interface';

export interface ApiResponse<T> {
  get: string;
  parameters: IParametersAPi[];
  errors: IErrorsAPi[];
  paging: IPagging;
  results: number;
  response: T
}

interface IPagging {
  current: number;
  total: number;
}

interface IParametersAPi {
  [key: string]: string;
}

interface IErrorsAPi {
  [key: string]: string;
}

export interface ITeamsResponse {
  team: ITeam;
  venue?: IVenue
}

export interface IPlayersResponse {
  player: IPlayer;
  statistics: IStatistics[]
}
