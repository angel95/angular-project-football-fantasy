export interface IPlayer{
  id: number,
  name: string,
  firstname: string | null,
  lastname: string | null,
  age: number,
  nationality: string | null,
  height: string | null,
  weight: string | null,
  photo: string
}

export interface IGamesStats {
  appearences: number,
  position: PlayerPosition,
}


export interface ITeamStats {
  id: number;
  name: string;
  logo: string;
}

export interface IStatistics{
  games: IGamesStats;
  team: ITeamStats;
}

export type PlayerPosition = 'Defender' | 'Goalkeeper' | 'Midfielder' | 'Attacker';
