export abstract class Mapper<I, O, C> {
  abstract mapFrom(param: I): O;

  abstract mapFromInterface(param: C): O;

}

