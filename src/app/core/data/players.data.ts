import { IPlayersResponse } from "../interfaces";

export const Players: IPlayersResponse[] = [
      {
        "player": {
          "id": 22090,
          "name": "W. Saliba",
          "firstname": "William Alain André Gabriel",
          "lastname": "Saliba",
          "age": 22,
          "nationality": "France",
          "height": "192 cm",
          "weight": "92 kg",
          "photo": "https://media-4.api-sports.io/football/players/22090.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 1145,
          "name": "I. Konaté",
          "firstname": "Ibrahima",
          "lastname": "Konaté",
          "age": 24,
          "nationality": "France",
          "height": "194 cm",
          "weight": "95 kg",
          "photo": "https://media-4.api-sports.io/football/players/1145.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 159,
          "name": "H. Lloris",
          "firstname": "Hugo Hadrien Dominique",
          "lastname": "Lloris",
          "age": 37,
          "nationality": "France",
          "height": "188 cm",
          "weight": "82 kg",
          "photo": "https://media-4.api-sports.io/football/players/159.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Goalkeeper",
            },
          }
        ]
      },
      {
        "player": {
          "id": 269,
          "name": "C. Nkunku",
          "firstname": "Christopher Alan",
          "lastname": "Nkunku",
          "age": 26,
          "nationality": "France",
          "height": "178 cm",
          "weight": "68 kg",
          "photo": "https://media-4.api-sports.io/football/players/269.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Attacker",
            },
          }
        ]
      },
      {
        "player": {
          "id": 22094,
          "name": "W. Fofana",
          "firstname": "Wesley Tidjan",
          "lastname": "Fofana",
          "age": 23,
          "nationality": "France",
          "height": "190 cm",
          "weight": "84 kg",
          "photo": "https://media-4.api-sports.io/football/players/22094.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 21998,
          "name": "A. Disasi",
          "firstname": "Axel Wilson Arthur",
          "lastname": "Disasi Mhakinis Belho",
          "age": 25,
          "nationality": "France",
          "height": "190 cm",
          "weight": "83 kg",
          "photo": "https://media-4.api-sports.io/football/players/21998.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },

          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
         },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          },
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },

          }
        ]
      },
      {
        "player": {
          "id": 742,
          "name": "R. Varane",
          "firstname": null,
          "lastname": null,
          "age": 30,
          "nationality": null,
          "height": null,
          "weight": null,
          "photo": "https://media-4.api-sports.io/football/players/742.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 277,
          "name": "M. Diaby",
          "firstname": "Moussa",
          "lastname": "Diaby",
          "age": 24,
          "nationality": "France",
          "height": "170 cm",
          "weight": "68 kg",
          "photo": "https://media-4.api-sports.io/football/players/277.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Attacker",
            },
          }
        ]
      },
      {
        "player": {
          "id": 1904,
          "name": "B. Kamara",
          "firstname": "Boubacar Bernard",
          "lastname": "Kamara",
          "age": 24,
          "nationality": "France",
          "height": "184 cm",
          "weight": "68 kg",
          "photo": "https://media-4.api-sports.io/football/players/1904.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Midfielder",
            },
          }
        ]
      },
      {
        "player": {
          "id": 253,
          "name": "A. Areola",
          "firstname": "Alphonse Francis",
          "lastname": "Areola",
          "age": 30,
          "nationality": "France",
          "height": "195 cm",
          "weight": "94 kg",
          "photo": "https://media-4.api-sports.io/football/players/253.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Goalkeeper",
            },
          }
        ]
      },
      {
        "player": {
          "id": 56,
          "name": "A. Griezmann",
          "firstname": "Antoine",
          "lastname": "Griezmann",
          "age": 32,
          "nationality": "France",
          "height": "176 cm",
          "weight": "73 kg",
          "photo": "https://media-4.api-sports.io/football/players/56.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Attacker",
            },
          }
        ]
      },
      {
        "player": {
          "id": 1257,
          "name": "J. Koundé",
          "firstname": "Jules Olivier",
          "lastname": "Koundé",
          "age": 25,
          "nationality": "France",
          "height": "180 cm",
          "weight": "75 kg",
          "photo": "https://media-4.api-sports.io/football/players/1257.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 1271,
          "name": "A. Tchouaméni",
          "firstname": "Aurélien Djani",
          "lastname": "Tchouaméni",
          "age": 23,
          "nationality": "France",
          "height": "187 cm",
          "weight": "81 kg",
          "photo": "https://media-4.api-sports.io/football/players/1271.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Midfielder",
            },
          }
        ]
      },
      {
        "player": {
          "id": 2207,
          "name": "E. Camavinga",
          "firstname": "Eduardo",
          "lastname": "Celmi Camavinga",
          "age": 21,
          "nationality": "France",
          "height": "182 cm",
          "weight": "68 kg",
          "photo": "https://media-4.api-sports.io/football/players/2207.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Midfielder",
            },
          }
        ]
      },
      {
        "player": {
          "id": 653,
          "name": "F. Mendy",
          "firstname": "Ferland Sinna",
          "lastname": "Mendy",
          "age": 28,
          "nationality": "France",
          "height": "180 cm",
          "weight": "73 kg",
          "photo": "https://media-4.api-sports.io/football/players/653.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 508,
          "name": "K. Coman",
          "firstname": "Kingsley Junior",
          "lastname": "Coman",
          "age": 27,
          "nationality": "France",
          "height": "181 cm",
          "weight": "76 kg",
          "photo": "https://media-4.api-sports.io/football/players/508.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Attacker",
            },
          }
        ]
      },
      {
        "player": {
          "id": 1149,
          "name": "D. Upamecano",
          "firstname": "Dayotchanculle Oswald",
          "lastname": "Upamecano",
          "age": 25,
          "nationality": "France",
          "height": "186 cm",
          "weight": "83 kg",
          "photo": "https://media-4.api-sports.io/football/players/1149.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 21509,
          "name": "M. Thuram",
          "firstname": "Marcus Lilian",
          "lastname": "Thuram-Ulien",
          "age": 26,
          "nationality": "France",
          "height": "192 cm",
          "weight": "90 kg",
          "photo": "https://media-4.api-sports.io/football/players/21509.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Attacker",
            },
          }
        ]
      },
      {
        "player": {
          "id": 2725,
          "name": "B. Pavard",
          "firstname": "Benjamin Jacques Marcel",
          "lastname": "Pavard",
          "age": 27,
          "nationality": "France",
          "height": "186 cm",
          "weight": "76 kg",
          "photo": "https://media-4.api-sports.io/football/players/2725.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 1,
              "position": "Defender",
            },
          }
        ]
      },
      {
        "player": {
          "id": 2295,
          "name": "O. Giroud",
          "firstname": "Olivier Jonathan",
          "lastname": "Giroud",
          "age": 37,
          "nationality": "France",
          "height": "193 cm",
          "weight": "91 kg",
          "photo": "https://media-4.api-sports.io/football/players/2295.png"
        },
        "statistics": [
          {
            "team": {
              "id": 2,
              "name": "France",
              "logo": "https://media-4.api-sports.io/football/teams/2.png"
            },
            "games": {
              "appearences": 0,
              "position": "Attacker",
            },
          }
        ]
      }

]
