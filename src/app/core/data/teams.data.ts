import { ITeamsResponse } from 'src/app/core/interfaces';
export const Data: ITeamsResponse[] = [
  {
    team: {
      id: 1,
      name: "Cape-Verde-Islands",
      code: "BEL",
      country: "Cape-Verde-Islands",
      founded: 1895,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/1.png"
    },
    venue: {
      id: 173,
      name: "Stade Roi Baudouin",
      address: "Avenue de Marathon 135/2, Laken",
      city: "Brussel",
      capacity: 50093,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/173.png"
    }
  },
  {
    team: {
      id: 2,
      name: "France",
      code: "FRA",
      country: "France",
      founded: 1919,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/2.png"
    },
    venue: {
      id: 631,
      name: "Stade de France",
      address: "Rue Jules Rimet",
      city: "Saint-Denis",
      capacity: 81338,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/631.png"
    }
  },
  {
    team: {
      id: 3,
      name: "Croatia",
      code: "CRO",
      country: "Croatia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/3.png"
    },
    venue: {
      id: 412,
      name: "Stadion Maksimir",
      address: "Maksimirska cesta 128",
      city: "Zagreb",
      capacity: 37168,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/412.png"
    }
  },
  {
    team: {
      id: 4,
      name: "Russia",
      code: "RUS",
      country: "Russia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/4.png"
    },
    venue: {
      id: 1327,
      name: "RZD Arena",
      address: "ul. Bol'shaya Cherkizovskaya 125",
      city: "Moskva",
      capacity: 28800,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1327.png"
    }
  },
  {
    team: {
      id: 5,
      name: "Sweden",
      code: "SWE",
      country: "Sweden",
      founded: 1904,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/5.png"
    },
    venue: {
      id: 1501,
      name: "Friends Arena",
      address: "Råsta Strandväg 1",
      city: "Solna",
      capacity: 54329,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1501.png"
    }
  },
  {
    team: {
      id: 6,
      name: "Brazil",
      code: "BRA",
      country: "Brazil",
      founded: 1914,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/6.png"
    },
    venue: {
      id: 204,
      name: "Estadio Jornalista Mário Filho (Maracanã)",
      address: "Rua Professor Eurico Rabelo, Maracanã",
      city: "Rio de Janeiro, Rio de Janeiro",
      capacity: 78838,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/204.png"
    }
  },
  {
    team: {
      id: 7,
      name: "Uruguay",
      code: "URU",
      country: "Uruguay",
      founded: 1900,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/7.png"
    },
    venue: {
      id: 1624,
      name: "Estadio Centenario",
      address: "Avenida Dr. Alfredo Navarro y Dr. Américo Ricaldoni, Parque Batlle",
      city: "Montevideo",
      capacity: 60235,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1624.png"
    }
  },
  {
    team: {
      id: 1,
      name: "Belgium",
      code: "BEL",
      country: "Belgium",
      founded: 1895,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/1.png"
    },
    venue: {
      id: 173,
      name: "Stade Roi Baudouin",
      address: "Avenue de Marathon 135/2, Laken",
      city: "Brussel",
      capacity: 50093,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/173.png"
    }
  },
  {
    team: {
      id: 2,
      name: "France",
      code: "FRA",
      country: "France",
      founded: 1919,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/2.png"
    },
    venue: {
      id: 631,
      name: "Stade de France",
      address: "Rue Jules Rimet",
      city: "Saint-Denis",
      capacity: 81338,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/631.png"
    }
  },
  {
    team: {
      id: 3,
      name: "Croatia",
      code: "CRO",
      country: "Croatia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/3.png"
    },
    venue: {
      id: 412,
      name: "Stadion Maksimir",
      address: "Maksimirska cesta 128",
      city: "Zagreb",
      capacity: 37168,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/412.png"
    }
  },
  {
    team: {
      id: 4,
      name: "Russia",
      code: "RUS",
      country: "Russia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/4.png"
    },
    venue: {
      id: 1327,
      name: "RZD Arena",
      address: "ul. Bol'shaya Cherkizovskaya 125",
      city: "Moskva",
      capacity: 28800,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1327.png"
    }
  },
  {
    team: {
      id: 5,
      name: "Sweden",
      code: "SWE",
      country: "Sweden",
      founded: 1904,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/5.png"
    },
    venue: {
      id: 1501,
      name: "Friends Arena",
      address: "Råsta Strandväg 1",
      city: "Solna",
      capacity: 54329,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1501.png"
    }
  },
  {
    team: {
      id: 6,
      name: "Brazil",
      code: "BRA",
      country: "Brazil",
      founded: 1914,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/6.png"
    },
    venue: {
      id: 204,
      name: "Estadio Jornalista Mário Filho (Maracanã)",
      address: "Rua Professor Eurico Rabelo, Maracanã",
      city: "Rio de Janeiro, Rio de Janeiro",
      capacity: 78838,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/204.png"
    }
  },
  {
    team: {
      id: 7,
      name: "Uruguay",
      code: "URU",
      country: "Uruguay",
      founded: 1900,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/7.png"
    },
    venue: {
      id: 1624,
      name: "Estadio Centenario",
      address: "Avenida Dr. Alfredo Navarro y Dr. Américo Ricaldoni, Parque Batlle",
      city: "Montevideo",
      capacity: 60235,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1624.png"
    }
  },
  {
    team: {
      id: 1,
      name: "Belgium",
      code: "BEL",
      country: "Belgium",
      founded: 1895,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/1.png"
    },
    venue: {
      id: 173,
      name: "Stade Roi Baudouin",
      address: "Avenue de Marathon 135/2, Laken",
      city: "Brussel",
      capacity: 50093,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/173.png"
    }
  },
  {
    team: {
      id: 2,
      name: "France",
      code: "FRA",
      country: "France",
      founded: 1919,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/2.png"
    },
    venue: {
      id: 631,
      name: "Stade de France",
      address: "Rue Jules Rimet",
      city: "Saint-Denis",
      capacity: 81338,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/631.png"
    }
  },
  {
    team: {
      id: 3,
      name: "Croatia",
      code: "CRO",
      country: "Croatia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/3.png"
    },
    venue: {
      id: 412,
      name: "Stadion Maksimir",
      address: "Maksimirska cesta 128",
      city: "Zagreb",
      capacity: 37168,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/412.png"
    }
  },
  {
    team: {
      id: 4,
      name: "Russia",
      code: "RUS",
      country: "Russia",
      founded: 1912,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/4.png"
    },
    venue: {
      id: 1327,
      name: "RZD Arena",
      address: "ul. Bol'shaya Cherkizovskaya 125",
      city: "Moskva",
      capacity: 28800,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1327.png"
    }
  },
  {
    team: {
      id: 5,
      name: "Sweden",
      code: "SWE",
      country: "Sweden",
      founded: 1904,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/5.png"
    },
    venue: {
      id: 1501,
      name: "Friends Arena",
      address: "Råsta Strandväg 1",
      city: "Solna",
      capacity: 54329,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1501.png"
    }
  },
  {
    team: {
      id: 6,
      name: "Brazil",
      code: "BRA",
      country: "Brazil",
      founded: 1914,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/6.png"
    },
    venue: {
      id: 204,
      name: "Estadio Jornalista Mário Filho (Maracanã)",
      address: "Rua Professor Eurico Rabelo, Maracanã",
      city: "Rio de Janeiro, Rio de Janeiro",
      capacity: 78838,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/204.png"
    }
  },
  {
    team: {
      id: 7,
      name: "Uruguay",
      code: "URU",
      country: "Uruguay",
      founded: 1900,
      national: true,
      logo: "https://media-4.api-sports.io/football/teams/7.png"
    },
    venue: {
      id: 1624,
      name: "Estadio Centenario",
      address: "Avenida Dr. Alfredo Navarro y Dr. Américo Ricaldoni, Parque Batlle",
      city: "Montevideo",
      capacity: 60235,
      surface: "grass",
      image: "https://media-4.api-sports.io/football/venues/1624.png"
    }
  }
]
