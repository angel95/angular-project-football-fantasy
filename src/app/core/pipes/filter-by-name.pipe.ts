import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {

  transform<T>(items: T[], searchText: string | null, fieldName: string): T[] {
    // return empty array if array is falsy
    if (!items) { return []; }

    // return the original array if search text is empty
    if (!searchText) { return items; }

    // convert the searchText to lower case
    searchText = searchText.toLowerCase();

    // retrun the filtered array
    return items.filter(item => {
      if (item && (item as any)[fieldName]) {
        return (item as any)[fieldName].toLowerCase().includes(searchText);
      }
      return false;
    });
  }
}


