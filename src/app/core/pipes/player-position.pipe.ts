import { Pipe, PipeTransform } from '@angular/core';
import { PlayerPosition } from '../interfaces';

@Pipe({
  name: 'playerPosition',
  standalone: true
})
export class PlayerPositionPipe implements PipeTransform {

  transform(value: PlayerPosition): string {
    let icon = 'sports_soccer';
    switch(value){
      case 'Midfielder':
        icon = 'timeline';
        break;
      case 'Defender':
        icon = 'security';
        break;
      case 'Attacker':
        icon = 'merge';
        break;
      case 'Goalkeeper':
        icon = 'front_hand';
        break;
      default:
        break;
      }
      return icon;
  }

}
