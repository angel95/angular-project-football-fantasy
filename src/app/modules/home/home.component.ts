import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { ApiResponse, IPlayersResponse, ITeamsResponse } from 'src/app/core/interfaces';
import { Data } from 'src/app/core/data/teams.data';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FilterByNamePipe } from 'src/app/core/pipes/filter-by-name.pipe';
import { Players } from 'src/app/core/data/players.data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
  ) {

  }

  ngOnInit(): void {
  }
}
