import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { TeamsModule } from '../teams/teams.module';
import { MyTeamModule } from '../my-team/my-team.module';
import { TeamSavedDirective } from 'src/app/core/directives/team-saved.directive';


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    TeamsModule,
    MyTeamModule,
    TeamSavedDirective
  ]
})
export class HomeModule { }
