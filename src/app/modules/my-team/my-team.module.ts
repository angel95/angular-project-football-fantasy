import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyTeamComponent } from './my-team.component';
import { PlayerListFilteredComponent } from './components/player-list-filtered/player-list-filtered.component';
import { SearchCoachComponent } from './components/search-coach/search-coach.component';
import { PersonItemComponent } from 'src/app/components/person-item/person-item.component';
import {MatDialogModule} from '@angular/material/dialog';
import { GeneralButtonComponent } from 'src/app/components/general-button/general-button.component';
import { TeamSavedDirective } from 'src/app/core/directives/team-saved.directive';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [
    MyTeamComponent,
    PlayerListFilteredComponent,
    SearchCoachComponent,

  ],
  imports: [
    CommonModule,
    PersonItemComponent,
    TeamSavedDirective,
    GeneralButtonComponent,
    MatDialogModule,
    SharedModule
  ],
  exports: [
    MyTeamComponent
  ]
})
export class MyTeamModule { }
