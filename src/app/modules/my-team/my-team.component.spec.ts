import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTeamComponent } from './my-team.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SharedModule } from '../shared-module/shared-module.module';
import { TeamsModule } from '../teams/teams.module';
import { MyTeamModule } from './my-team.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MyTeamComponent', () => {
  let component: MyTeamComponent;
  let fixture: ComponentFixture<MyTeamComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyTeamComponent],
      imports: [MatDialogModule, SharedModule, TeamsModule, MyTeamModule, BrowserAnimationsModule]
    });
    fixture = TestBed.createComponent(MyTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
