import { Component, Input, inject } from '@angular/core';
import { PlayerPosition } from '../../../../core/interfaces/player.interface';
import { PlayerApp } from '../../../../core/models/player/player.model';
import { StateService } from 'src/app/core/services/state-service.service';

@Component({
  selector: 'app-player-list-filtered',
  templateUrl: './player-list-filtered.component.html',
  styleUrls: ['./player-list-filtered.component.scss']
})
export class PlayerListFilteredComponent  {

  @Input() playerAppArr!: PlayerApp[];
  @Input() playerPosition!: PlayerPosition;
  @Input() minPlayers!: number;

  private stateService = inject(StateService);

  constructor(){

  }

  removePlayer(player: PlayerApp): void{
    this.stateService.removePlayerFromTeam(player)
  }

}
