import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerListFilteredComponent } from './player-list-filtered.component';
import {  MatSnackBarModule } from '@angular/material/snack-bar';

// const arr = [];

describe('PlayerListFilteredComponent', () => {
  let component: PlayerListFilteredComponent;
  let fixture: ComponentFixture<PlayerListFilteredComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      declarations: [PlayerListFilteredComponent]
    });
    fixture = TestBed.createComponent(PlayerListFilteredComponent);
    component = fixture.componentInstance;
    component.playerAppArr = []
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
