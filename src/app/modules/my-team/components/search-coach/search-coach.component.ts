import { Component, OnInit, inject, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { debounceTime, distinctUntilChanged, Subject, takeUntil, BehaviorSubject } from 'rxjs';
import { ApiResponse, ICoach } from 'src/app/core/interfaces';
import { FootballApiService } from 'src/app/core/services/football-api-service.service';
import { StateService } from 'src/app/core/services/state-service.service';

@Component({
  selector: 'app-search-coach',
  templateUrl: './search-coach.component.html',
  styleUrls: ['./search-coach.component.scss']
})
export class SearchCoachComponent implements OnInit, OnDestroy{
  form: FormGroup;
  coachs: ICoach[] = [];
  destroyed$ = new Subject();
  loading = false;
  notFound = false;
  private footballApiService = inject(FootballApiService);
  private stateService = inject(StateService);
  constructor(private builder: FormBuilder, private dialogRef: MatDialog) {
    this.form = builder.group({
      search: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }
  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();

  }

  ngOnInit(): void {
      this.form.controls['search'].valueChanges.pipe(debounceTime(500), distinctUntilChanged(), takeUntil(this.destroyed$)).subscribe((val) =>{
        this.notFound = false;
        if(val.length < 4){
          this.coachs = [];
          return;
        }
        this.loading = true;

        this.footballApiService.getCoachByName(val).pipe(takeUntil(this.destroyed$)).subscribe((response: ApiResponse<ICoach[]>) => {
          this.loading = false;
          if(response.response.length == 0){
            this.notFound = true;
          }
          this.coachs = response.response;
        });
      });
    // throw new Error('Method not impl0emented.');
  }

  selectCoach(coach: ICoach){
    this.stateService.addCoach(coach);
    this.dialogRef.closeAll();
  }

}
