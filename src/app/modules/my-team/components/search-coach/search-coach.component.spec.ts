import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCoachComponent } from './search-coach.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/modules/shared-module/shared-module.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SearchCoachComponent', () => {
  let component: SearchCoachComponent;
  let fixture: ComponentFixture<SearchCoachComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchCoachComponent],
      imports: [MatDialogModule, HttpClientModule, SharedModule, BrowserAnimationsModule]
    });
    fixture = TestBed.createComponent(SearchCoachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
