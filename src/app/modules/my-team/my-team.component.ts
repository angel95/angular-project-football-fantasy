import { TeamApp } from './../../core/models/team.model';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StateService } from 'src/app/core/services/state-service.service';
import { Subject, takeUntil, debounceTime, distinctUntilChanged } from 'rxjs';
import { PlayerApp } from '../../core/models/player/player.model';
import {MatDialog} from '@angular/material/dialog';
import { SearchCoachComponent } from './components/search-coach/search-coach.component';
import { ICoach } from 'src/app/core/interfaces';

@Component({
  selector: 'app-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.scss']
})
export class MyTeamComponent implements OnInit, OnDestroy{
  form: FormGroup;
  teamApp?: TeamApp;
  private stateService = inject(StateService);
  private detroy$ = new Subject();
  constructor(private builder: FormBuilder, public dialog: MatDialog) {
    this.form = builder.group({
      name: ['']
    });
  }

  ngOnDestroy(): void {
    this.detroy$.next(true);
    this.detroy$.unsubscribe();
  }

  ngOnInit(): void {
    this.stateService.myTeam$.pipe(takeUntil(this.detroy$),).subscribe((team: TeamApp) => {
      this.teamApp = team;
      if(this.form.controls['name'].pristine){
        this.form.controls['name'].setValue(team.name);
      }
    });

    this.form.controls['name'].valueChanges.pipe(takeUntil(this.detroy$), debounceTime(300), distinctUntilChanged()).subscribe((teamName: string) => {
      this.stateService.setTeamName(teamName);
    })
  }

  openDialog() {
     this.dialog.open(SearchCoachComponent);
  }

  removeCoach(coach: ICoach){
    this.stateService.removeCoachFromTeam()
  }

  removePlayer(player: PlayerApp): void{
    this.stateService.removePlayerFromTeam(player)
  }

  save(): void {
    this.stateService.saveTeam();
  }

  edit(): void {
    this.stateService.editTeam();
  }

  delete(): void {
    this.stateService.deleteTeam();
  }
}
