import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsComponent } from './teams.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { TeamsModule } from './teams.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TeamsComponent', () => {
  let component: TeamsComponent;
  let fixture: ComponentFixture<TeamsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamsComponent],
      imports: [SharedModule, TeamsModule, HttpClientModule, BrowserAnimationsModule]
    });
    fixture = TestBed.createComponent(TeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
