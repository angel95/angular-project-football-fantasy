import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSelectionComponent } from './team-selection.component';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from 'src/app/core/core.module';
import { TeamsModule } from '../../teams.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/modules/shared-module/shared-module.module';

describe('TeamSelectionComponent', () => {
  let component: TeamSelectionComponent;
  let fixture: ComponentFixture<TeamSelectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamSelectionComponent],
      imports: [HttpClientModule, CoreModule, TeamsModule, SharedModule, BrowserAnimationsModule]
    });
    fixture = TestBed.createComponent(TeamSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
