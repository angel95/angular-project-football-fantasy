import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Players } from 'src/app/core/data/players.data';
import { Data } from 'src/app/core/data/teams.data';
import { ApiResponse, IPlayersResponse, ITeam, ITeamsResponse } from 'src/app/core/interfaces';
import { FootballApiService } from 'src/app/core/services/football-api-service.service';
import { Subject, from, interval, takeUntil, zip } from 'rxjs';

@Component({
  selector: 'team-selection',
  templateUrl: './team-selection.component.html',
  styleUrls: ['./team-selection.component.scss']
})
export class TeamSelectionComponent implements OnDestroy {
  form: FormGroup;
  destroyed = new Subject();

  constructor(
    private footballApiService: FootballApiService,
    private builder: FormBuilder
  ) {
    this.form = builder.group({
      name: ['']
    });
  }
  data: ITeam[] = [];
  //for testing
  // data = Data.map((teamsResponse: ITeamsResponse) => {
  //   return teamsResponse.team;
  // });

  ngOnInit(): void {
    this.footballApiService.getAllTeams().pipe(takeUntil(this.destroyed))
      .subscribe((teamsResponse: ApiResponse<ITeamsResponse[]>) => {
        const arrTeams: ITeam[] =  teamsResponse.response.map((responseTeam) => {
            return responseTeam.team;
          });
        this.data = arrTeams;
    });
  }

  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }

}
