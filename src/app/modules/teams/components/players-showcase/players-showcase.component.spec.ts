import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayersShowcaseComponent } from './players-showcase.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PlayersShowcaseComponent', () => {
  let component: PlayersShowcaseComponent;
  let fixture: ComponentFixture<PlayersShowcaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayersShowcaseComponent],
      imports: [MatSnackBarModule, HttpClientModule, MatIconModule, MatFormFieldModule, MatGridListModule, FormsModule, ReactiveFormsModule, InfiniteScrollModule, MatInputModule, BrowserAnimationsModule]
    });
    fixture = TestBed.createComponent(PlayersShowcaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
