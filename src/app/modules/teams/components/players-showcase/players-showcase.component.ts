import { Component, inject, OnInit, OnDestroy } from '@angular/core';
import { Players } from 'src/app/core/data/players.data';
import { IPlayer, IPlayersResponse, ITeam } from 'src/app/core/interfaces';
import { PlayerMapper } from 'src/app/core/models/player/player.mapper';
import { StateService } from 'src/app/core/services/state-service.service';
import { PlayerApp } from '../../../../core/models/player/player.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Subject, combineLatest, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs';
import { FootballApiService } from 'src/app/core/services/football-api-service.service';
import { ApiResponse } from '../../../../core/interfaces/api-response.interface';

@Component({
  selector: 'team-players-showcase',
  templateUrl: './players-showcase.component.html',
  styleUrls: ['./players-showcase.component.scss']
})
export class PlayersShowcaseComponent implements OnInit, OnDestroy{
  form: FormGroup;
  private stateService = inject(StateService);
  private footballApiService = inject(FootballApiService);
  private page$ = new BehaviorSubject<number>(1);
  private maxPage = 999;
  private actualTeam$ = new BehaviorSubject<ITeam | null>(null);
  private search$ = new BehaviorSubject<string>('');
  public players: PlayerApp[] = [];
  notFound = false;
  minLengthError = false;
  loading = false;
  playerMapper = new PlayerMapper();
  destroy$ = new Subject();


  constructor(private builder: FormBuilder) {
    this.form = builder.group({
      name: ['', Validators.compose([Validators.minLength(4),])]

    });
  }

  ngOnInit(): void {
    this.stateService.teamSelected$.pipe(takeUntil(this.destroy$)).subscribe((team: ITeam | null) => {
      this.players = [];
      this.actualTeam$.next(team);

      if(team == null){
        return;
      }

    })

    this.form.controls['name'].valueChanges.pipe(debounceTime(500), distinctUntilChanged(), takeUntil(this.destroy$)).subscribe((val) =>{
      if(val.length < 4 && val.length > 0){
        this.minLengthError = true;
        this.players = [];
        return;
      }
      this.minLengthError = false;

      this.players = [];
      this.search$.next(val);
    });


    combineLatest([this.page$, this.actualTeam$, this.search$]).pipe(takeUntil(this.destroy$)).subscribe(async (value) => {
      const [page, actualTeam, search] = value;
      if(actualTeam == null){
        return;
      }
      this.notFound = false;

      this.loading = true;
      const apiData: ApiResponse<IPlayersResponse[]> = await this.footballApiService.getPlayersByTeamPromise(actualTeam, page, search);
      const auxPlayersArray: PlayerApp[] = apiData.response.map((playersResponse: IPlayersResponse) => {
            return this.playerMapper.mapFrom(playersResponse);
      });
      this.players.push(...auxPlayersArray);
      this.loading = false
      if(this.players.length == 0){
        this.notFound = true;
      }
      this.loading = false;
      return;

    })
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onScroll= ()=>{
    this.page$.next(this.page$.value + 1);
   }

  public backToTeams(){
    this.actualTeam$.next(null);
    this.players = [];
    this.form.controls['name'].setValue('');
    this.page$.next(1);
    this.stateService.switchStateTeamSelection();
  }

  public addItemToUserTeam(player: PlayerApp) {
    this.stateService.addPlayerToTeam(player);
  }
}
