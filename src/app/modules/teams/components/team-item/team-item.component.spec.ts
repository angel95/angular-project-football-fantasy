import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamItemComponent } from './team-item.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { ITeam } from 'src/app/core/interfaces';

const teamMock: ITeam = {
    id: 1,
    name: "Cape-Verde-Islands",
    code: "BEL",
    country: "Cape-Verde-Islands",
    founded: 1895,
    national: true,
    logo: "https://media-4.api-sports.io/football/teams/1.png"
}

describe('TeamItemComponent', () => {
  let component: TeamItemComponent;
  let fixture: ComponentFixture<TeamItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamItemComponent],
      imports: [MatSnackBarModule, MatCardModule]
    });
    fixture = TestBed.createComponent(TeamItemComponent);
    component = fixture.componentInstance;
    component.team = teamMock;
    fixture.detectChanges();
  });



  it('should create', () => {
    // component.selectTeam = ;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
