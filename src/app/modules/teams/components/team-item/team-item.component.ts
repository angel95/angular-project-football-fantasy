import { Component, Input, inject } from '@angular/core';
import { ITeam } from 'src/app/core/interfaces';
import { PlayerApp } from 'src/app/core/models/player/player.model';
import { StateService } from 'src/app/core/services/state-service.service';

@Component({
  selector: 'team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.scss']
})
export class TeamItemComponent {
  @Input () team!: ITeam;

  private stateService = inject(StateService);

  selectTeam(){
    this.stateService.setTeam(this.team);
    this.stateService.switchStateTeamSelection();
  }

}
