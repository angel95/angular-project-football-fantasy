import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './teams.component';
import { CoreModule } from 'src/app/core/core.module';
import { TeamSelectionComponent } from './components/team-selection/team-selection.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { TeamItemComponent } from './components/team-item/team-item.component';
import { SlidePanelComponent } from './components/slide-panel/slide-panel.component';
import { PlayersShowcaseComponent } from './components/players-showcase/players-showcase.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PersonItemComponent } from 'src/app/components/person-item/person-item.component';
import { SharedModule } from '../shared-module/shared-module.module';



@NgModule({
  declarations: [TeamsComponent, TeamSelectionComponent, TeamItemComponent, SlidePanelComponent, PlayersShowcaseComponent, ],
  imports: [
    CommonModule,
    CoreModule,
    PersonItemComponent,
    InfiniteScrollModule,
    SharedModule
  ],
  exports: [TeamsComponent]
})
export class TeamsModule { }
