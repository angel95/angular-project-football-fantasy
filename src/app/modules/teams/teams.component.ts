import { Component, inject } from '@angular/core';
import { StateService } from '../../core/services/state-service.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent {
  private readonly stateService = inject(StateService)

  isLeftVisible = true;
  isLeftVisible$ = this.stateService.isTeamSelected$;

  switchState(){
    this.stateService.switchStateTeamSelection();
  }
}
