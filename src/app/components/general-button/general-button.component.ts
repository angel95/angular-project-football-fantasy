import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: ']general-button',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './general-button.component.html',
  styleUrls: ['./general-button.component.scss']
})
export class GeneralButtonComponent {

  // @Input() action!: Function;
  @Input() text!: string;
  @Input() color: string = 'primary';

}
