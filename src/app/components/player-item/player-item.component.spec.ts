import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerItemComponent } from './player-item.component';
import { PlayerApp } from 'src/app/core/models/player/player.model';

const playerMock: PlayerApp = new PlayerApp(
  23,
  22094,
  'W. Fofana',
  'https://media-4.api-sports.io/football/players/22094.png"',
  'Defender',
  'France',
  'https://media-4.api-sports.io/football/teams/2.png'
)

describe('PlayerItemComponent', () => {
  let component: PlayerItemComponent;
  let fixture: ComponentFixture<PlayerItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PlayerItemComponent]
    });
    fixture = TestBed.createComponent(PlayerItemComponent);
    component = fixture.componentInstance;
    component.player = playerMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    // component.player = playerMock;
    expect(component).toBeTruthy();
  });
});
