import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerApp } from 'src/app/core/models/player/player.model';
import {MatIconModule} from '@angular/material/icon'
import { PlayerPositionPipe } from 'src/app/core/pipes/player-position.pipe';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'player-item',
  standalone: true,
  imports: [CommonModule, MatIconModule, PlayerPositionPipe, MatCardModule],
  templateUrl: './player-item.component.html',
  styleUrls: ['./player-item.component.scss']
})
export class PlayerItemComponent {
  @Input () player!: PlayerApp;

}
