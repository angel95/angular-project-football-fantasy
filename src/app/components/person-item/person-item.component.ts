import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerApp } from 'src/app/core/models/player/player.model';
import { ICoach } from 'src/app/core/interfaces';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { PlayerPositionPipe } from 'src/app/core/pipes/player-position.pipe';

@Component({
  selector: 'person-item',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatIconModule, PlayerPositionPipe],
  templateUrl: './person-item.component.html',
  styleUrls: ['./person-item.component.scss']
})
export class PersonItemComponent {
  @Input () person!: PlayerApp | ICoach;

}
