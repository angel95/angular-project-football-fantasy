import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonItemComponent } from './person-item.component';
import { ICoach } from '../../core/interfaces/coach.interface';

const person: ICoach = {
    "id": 4,
    "name": "Guardiola",
    "firstname": "Josep",
    "lastname": "Guardiola i Sala",
    "age": 52,
    "nationality": "Spain",
    "height": "180 cm",
    "weight": "70 kg",
    "photo": "https://media-4.api-sports.io/football/coachs/4.png",
    position: null
}


describe('PersonItemComponent', () => {
  let component: PersonItemComponent;
  let fixture: ComponentFixture<PersonItemComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PersonItemComponent]
    });
    fixture = TestBed.createComponent(PersonItemComponent);
    component = fixture.componentInstance;
    component.person = person;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.person = person;
    fixture.detectChanges(); // 2
    expect(component).toBeTruthy();
  });
});
