import { Component, OnInit, inject } from '@angular/core';
import { StateService } from './core/services/state-service.service';
import { LocalStorageService } from './core/services/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  private stateService = inject(StateService);
  private localStorageService = inject(LocalStorageService);

  title = 'football-fantasy';

  ngOnInit(): void {
    const teamdata = this.checkStorageForTeamDraft();
    if(teamdata != null){
      this.stateService.getTeamFromStorage(teamdata);
    }
  }


  checkStorageForTeamDraft(){
    const data = this.localStorageService.getItem('myTeam');
    return data;
  }
}
